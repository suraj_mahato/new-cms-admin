package com.new_cms_admin.qa.testcases;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.new_cms_admin.qa.base.TestBase;
import com.new_cms_admin.qa.listeners.AllureReportListeners;
import com.new_cms_admin.qa.pages.LoginPage;
import com.new_cms_admin.qa.utilities.CreateEnvFile;
import com.new_cms_admin.qa.utilities.LoginCredentialsUtils;
import com.new_cms_admin.qa.utilities.SetGetParameter;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Link;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({ AllureReportListeners.class })
@Feature("Login Page Test")
@Epic("Functional Testing")
public class LoginPageTest extends TestBase {

	LoginPage loginPage;

	String sheetName = "loginCredentails";
	
	public SetGetParameter setGetParamter ;
	
	public CreateEnvFile createEnvFile ;

	public LoginPageTest() throws IOException {
		super();

	}

	@BeforeMethod(description = "Browser Start Up")
	public void setUp() {

		initiallization();

		loginPage = new LoginPage();

	}

	/*
	 * Sceanrio for login page title
	 */


	@Test(enabled = false, priority = 0, description = "Verify for login page title")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate login page title")
	@Story("Story Name : To check LoginPage Title")
	public void validateLoginPageTest() throws Exception {

		String title = loginPage.validateLoginPageTitle();

		Assert.assertEquals(title, "CD CMS Portal");

	}

	/*
	 * verify logo image
	 */

	@Test(enabled = false, priority = 1, description = "verify logo image ")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate logo image")
	@Story("Story Name : To check Logo image")
	public void validateCDLogoImageTest() throws Exception {

		boolean flag = loginPage.validateCDLogoImage();

		Assert.assertTrue(flag);

	}

	@DataProvider
	public Object[][] cdTestData() {
		Object data[][] = LoginCredentialsUtils.getTestData(sheetName);
		return data;

	}

	@Test(priority = 2,dataProvider = "cdTestData", description = "Verifying login functionality with valid and invalid username and password")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: Verifying login functionality with valid and invalid username and password")
	@Story("Story Name : To check and Verify login functionality with valid and invalid username and password")
	public void loginCredentails(String Username, String Password) throws Exception {

		// Created list for Customer Details
		List<String> elements = new ArrayList<String>();

		// Enter username
		elements.add(Username);

		// enter password
		elements.add(Password);

		loginPage.loginCredentails(elements);

	try {
		
		// click on login Button
				loginPage.clickOnLoginButton();
				
				Thread.sleep(2000);

				String title = loginPage.validateHomePageText();

				Assert.assertEquals(title, "Welcome to Country Delignt!");
		
	}
	
	catch(Exception e) {
		
		 Assert.assertFalse(true, "Validation Message for UserName and Password, please enter valid password., Oops, Looks like you are not registered with us. Please sign up.\n");
		
	}	

	}

	@AfterMethod(description = "Browser Tear Down")
	public void End() {
		
			driver.quit();
			driver=null;

		}

	}
	
	
