package com.new_cms_admin.qa.testcases;

import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.new_cms_admin.qa.base.TestBase;
import com.new_cms_admin.qa.listeners.AllureReportListeners;
import com.new_cms_admin.qa.pages.CustomerProfilePage;
import com.new_cms_admin.qa.pages.LoginPage;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Link;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({ AllureReportListeners.class })
@Feature("Customer Search Test")
@Epic("End To End Testing")
public class CustomerProfilePageTest extends TestBase {

	LoginPage loginPage;
	CustomerProfilePage customerProfilePage;


	public CustomerProfilePageTest() throws IOException {
		super();

	}

	@BeforeMethod(description = "Browser Start Up")
	public void setUp() throws Exception {

		initiallization();
		
		customerProfilePage =new CustomerProfilePage();

		loginPage = new LoginPage();
		
		customerProfilePage=loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		

	}

	/*
	 * Customer profile functionality
	 * 
	 * 
	 */

	@Test(enabled=false,priority = 0,description = "Verify that customer search page")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate customer search page")
	@Story("Story Name : To check customer search page should be open")
	public void validateCustomerSearchPage() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();

		String title = customerProfilePage.validateCustomerSearchPage();
		Assert.assertEquals(title, "CUSTOMER > SEARCH");
		
		Thread.sleep(6000);

	}


	@Test(enabled=false,priority = 1,description = "Verify that selecting any route randomly")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate selecting any route randomly")
	@Story("Story Name : To check selecting any route")
	public void validateselectinganyRoute() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);

		customerProfilePage.validateselectinganyRandomRoute();
		
		Thread.sleep(6000);

	}
	
	@Test(enabled=false,priority = 2,description = "Verify that selecting any route randomly first then searching the customer list for selected routes")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate selecting any route randomly first then searching the customer list for selected routes")
	@Story("Story Name : To check Searching customer list for selected route ")
	public void validateselectinganyRoutesandSearching() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		//click on randomly any route
		Thread.sleep(5000);
		customerProfilePage.validateselectinganyRandomRoute();
		
		Thread.sleep(3000);
		
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		String expectedResult = "Selected route customer list should be display ";
		String ActualResult= "Selected route customer list should be display ";
		Assert.assertEquals(ActualResult, expectedResult);
		
		Thread.sleep(6000);

	}
	
	@Test(enabled=false,priority = 3,description = "Verify that selecting any Area randomly first then searching the customer list for selected Area")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate selecting any area randomly first then searching the customer list for selected Area")
	@Story("Story Name : To check Searching customer list for selected area ")
	public void validateselectinganyAreaandSearching() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		//click on randomly any area
		Thread.sleep(5000);
		customerProfilePage.validateselectinganyRandomArea();
		
		Thread.sleep(3000);
		
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		String expectedResult = "Selected area customer list should be display ";
		String ActualResult= "Selected area customer list should be display ";
		Assert.assertEquals(ActualResult, expectedResult);
		
		Thread.sleep(6000);

	}
	
	@Test(enabled=false,priority = 4, description = "Verify that Search with valid Name for customer")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Name for customer")
	@Story("Story Name : To check Search with valid Name for customer")
	public void validateSearchWithName() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter valid Name
		customerProfilePage.validatesSearchWithName(prop.getProperty("validName"));
		
		Thread.sleep(4000);
		
		//click on customer button
		customerProfilePage.clickOnSearchButton();
	

		String expectedResult = "Sorted customer list should be display ";
		String ActualResult= "Sorted customer list should be display ";
		Assert.assertEquals(ActualResult, expectedResult);
		
		Thread.sleep(6000);
	
	}
	
	@Test(enabled=false,priority = 5, description = "Verify that Search with invalid Name(Customer Name is enter wrong) for customer")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with invalid Name(Customer Name is enter wrong) for customer")
	@Story("Story Name : To check Search with invalid Name(Customer Name is enter wrong) for customer")
	public void validateSearchWithInvalidName() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter valid Name
		customerProfilePage.validatesSearchWithName(prop.getProperty("invalidName"));
		
		Thread.sleep(4000);
		
		//click on customer button
		customerProfilePage.clickOnSearchButton();
	

		String expectedResult = "No customer found";
		String ActualResult= customerProfilePage.validateValidationMessage();
		
		Assert.assertEquals(ActualResult, expectedResult, "Validation message not display");
		
		Thread.sleep(6000);


	}
	
	@Test(enabled=false,priority = 6, description = "Verify that Search with valid Phone Number for customer")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer")
	@Story("Story Name : To check Search with valid Phone Number for customer")
	public void validateSearchWithPhoneNumber() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(4000);
		
		//click on customer button
		customerProfilePage.clickOnSearchButton();
	

		String expectedResult = "Sorted customer list should be display ";
		String ActualResult= "Sorted customer list should be display ";
		Assert.assertEquals(ActualResult, expectedResult);
		
		Thread.sleep(6000);

	}
	
	@Test(enabled=false,priority = 7, description = "Verify that Search with invalid Phone Number(Customer phone number is enter wrong) for customer")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with invalid Phone Number(Customer phone number is enter wrong) for customer")
	@Story("Story Name : To check Search with invalid Phone Number(Customer phone number is enter wrong) for customer")
	public void validateSearchWithInvalidPhoneNumber() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter valid name
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("invalidPhone"));
		
		Thread.sleep(4000);
		
		//click on customer button
		customerProfilePage.clickOnSearchButton();
	

		String expectedResult = "No customer found";
		String ActualResult= customerProfilePage.validateValidationMessage();
		
		Assert.assertEquals(ActualResult, expectedResult, "Validation message not display");
		
		Thread.sleep(6000);

	}
	
	@Test(enabled=false,priority = 8, description = "Verify that Search with valid customer Id for customer")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid customer Id for customer")
	@Story("Story Name : To check Search with valid customer Id for customer")
	public void validateSearchWithCustomerId() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithCustomerId(prop.getProperty("customerId"));
		
		Thread.sleep(4000);
		
		//click on search button
		customerProfilePage.clickOnSearchButton();
	

		String expectedResult = "Sorted customer list should be display ";
		String ActualResult= "Sorted customer list should be display ";
		Assert.assertEquals(ActualResult, expectedResult);
		
		Thread.sleep(6000);

	}
	
	@Test(enabled=false,priority = 9, description = "Verify that Search with invalid customer Id for customer")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with invalid customer Id for customer")
	@Story("Story Name : To check Search with invalid customer Id for customer")
	public void validateSearchWithinvalidCustomerId() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithCustomerId(prop.getProperty("invalidcustomerId"));
		
		Thread.sleep(4000);
		
		//click on customer button
		customerProfilePage.clickOnSearchButton();
	

		String expectedResult = "No customer found";
		String ActualResult= customerProfilePage.validateValidationMessage();
		
		Assert.assertEquals(ActualResult, expectedResult, "Validation message not display");
		
		Thread.sleep(6000);
		
		

	}
	
	@Test(enabled=false,priority = 10, description = "Verify that Search with valid Address for customer")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Address for customer")
	@Story("Story Name : To check Search with valid Address for customer")
	public void validateSearchWithvalidAddress() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter valid address
		customerProfilePage.validatesSearchWithAddress(prop.getProperty("validAddress"));
		
		Thread.sleep(4000);
		
		//click on customer button
		customerProfilePage.clickOnSearchButton();
	
		String expectedResult = "Sorted customer list should be display ";
		String ActualResult= "Sorted customer list should be display ";
		Assert.assertEquals(ActualResult, expectedResult);
		
		Thread.sleep(6000);
		
		

	}
	
	@Test(enabled=false,priority = 11, description = "Verify that Search with invalid Address for customer")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with invalid Address for customer")
	@Story("Story Name : To check Search with invalid Address for customer")
	public void validateSearchWithinvalidAddress() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter invalid address
		customerProfilePage.validatesSearchWithAddress(prop.getProperty("invalidAddress"));
		
		Thread.sleep(4000);
		
		//click on customer button
		customerProfilePage.clickOnSearchButton();
	

		String expectedResult = "No customer found";
		String ActualResult= customerProfilePage.validateValidationMessage();
		
		Assert.assertEquals(ActualResult, expectedResult, "Validation message not display");
		
		Thread.sleep(6000);
		
		

	}
	
	@Test(enabled=false,priority = 12, description = "Verify that Search with valid Email for customer")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Email for customer")
	@Story("Story Name : To check Search with valid Email for customer")
	public void validateSearchWithvalidEmail() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter valid address
		customerProfilePage.validatesSearchWithEmail(prop.getProperty("validEmail"));
		
		Thread.sleep(4000);
		
		//click on customer button
		customerProfilePage.clickOnSearchButton();
	
		String expectedResult = "Sorted customer list should be display ";
		String ActualResult= "Sorted customer list should be display ";
		Assert.assertEquals(ActualResult, expectedResult);
		
		Thread.sleep(6000);
		
		

	}


	
	@Test(enabled=false,priority = 13, description = "Verify that Search with invalid Email for customer")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with invalid Email for customer")
	@Story("Story Name : To check Search with invalid Email for customer")
	public void validateSearchWithinvalidEmail() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter valid address
		customerProfilePage.validatesSearchWithEmail(prop.getProperty("invalidEmail"));
		
		Thread.sleep(4000);
		
		//click on customer button
		customerProfilePage.clickOnSearchButton();
	
		String expectedResult = "Search button is disabled ";
		String ActualResult= "Search button is disabled ";
		Assert.assertEquals(ActualResult, expectedResult);
		
		Thread.sleep(6000);
		
		

	}
	
	@Test(enabled=false,priority = 14,description = "Verify that selecting any route randomly first and also selecting any Area randomly then searching the customer list for selected routes")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate selecting any route randomly first and also selecting any Area randomly then searching the customer list for selected routes")
	@Story("Story Name : To check Searching customer list for selected Route and Area ")
	public void validateselectinganyRoutesandAreaandSearching() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		//click on randomly any route
		Thread.sleep(5000);
		customerProfilePage.validateselectinganyRandomRoute();
		
		//click on randomly any area
		Thread.sleep(2000);
		customerProfilePage.validateselectinganyRandomArea();
		
		Thread.sleep(3000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		String expectedResult = "Selected route customer list should be display ";
		String ActualResult= "Selected route customer list should be display ";
		Assert.assertEquals(ActualResult, expectedResult);
		
		Thread.sleep(6000);

	}
	
	
/*
 * Integration testing
*/
	
	@Test(enabled=false,priority = 15,description = "Verify that selecting any route randomly first and also selecting any Area randomly then searching the customer list for selected routes")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate selecting any route randomly first and also selecting any Area randomly then searching the customer list for selected routes")
	@Story("Story Name : To check Searching customer list for selected Route and Area ")
	public void validateselectingandEnterAllFiledSearching() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		//click on randomly any route
		Thread.sleep(5000);
		customerProfilePage.validateselectinganyRandomRoute();
		
		//click on randomly any area
		Thread.sleep(2000);
		customerProfilePage.validateselectinganyRandomArea();
		
		//Enter valid Name
		customerProfilePage.validatesSearchWithName(prop.getProperty("validName"));
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		//Enter customerId
		customerProfilePage.validatesSearchWithCustomerId(prop.getProperty("customerId"));
		
		//Enter valid address
		customerProfilePage.validatesSearchWithAddress(prop.getProperty("validAddress"));
		
		//Enter valid address
		customerProfilePage.validatesSearchWithEmail(prop.getProperty("validEmail"));
		
		Thread.sleep(3000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		String expectedResult = "No customer found";
		String ActualResult= customerProfilePage.validateValidationMessage();
		
		Assert.assertEquals(ActualResult, expectedResult, "Validation message not display");
		
		Thread.sleep(6000);

	}
	
	@Test(enabled=false,priority = 16,description = "Verify that Search with valid Phone Number for customer and clicking it.")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it.")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it.")
	public void validateselectingCustomer() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();

	
		Thread.sleep(6000);
		
	}
	
	
	@Test(enabled=false,priority = 17,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter Valid all Upper case in first and Last Name Fileds")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter Valid all Upper case in first and Last Name Fileds")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter Valid all Upper case in first and Last Name Fileds")
	public void validateEnterValidallUppercaseFirstLastName() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//enter first and last name
		Thread.sleep(4000);
		customerProfilePage.validatesEnterFirstandLastName(prop.getProperty("ValidUpperCaseFirstName"),prop.getProperty("ValidUpperCaseLastName"));
		
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		String expectedResult = "Customer updated successfully!";
		String ActualResult= "Customer updated successfully!";
		Assert.assertEquals(ActualResult, expectedResult);
			
		Thread.sleep(7000);

  }
	
	@Test(enabled=false,priority = 18,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter Valid all lower case in first and Last Name Fileds")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter Valid all lower case in first and Last Name Fileds")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter Valid all lower case in first and Last Name Fileds")
	public void validateEnterValidAllLowercaseFirstLastName() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//enter first and last name
		Thread.sleep(4000);
		customerProfilePage.validatesEnterFirstandLastName(prop.getProperty("ValidLowerCaseFirstName"),prop.getProperty("ValidLowerCaseLastName"));
		
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		String expectedResult = "Customer updated successfully!";
		String ActualResult= "Customer updated successfully!";
		Assert.assertEquals(ActualResult, expectedResult);
		
		Thread.sleep(7000);

  }
	
	@Test(enabled=false,priority = 19,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter Valid Upper lower case in first and Last Name Fileds")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter Valid Upper lower case in first and Last Name Fileds")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter Valid Upper lower case in first and Last Name Fileds")
	public void validateEnterValidUpperLowercaseFirstLastName() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//enter first and last name
		Thread.sleep(4000);
		customerProfilePage.validatesEnterFirstandLastName(prop.getProperty("ValidUpperLowerCaseFirstName"),prop.getProperty("ValidUpperLowerCaseLastName"));
		
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		String expectedResult = "Customer updated successfully!";
		String ActualResult= "Customer updated successfully!";
		Assert.assertEquals(ActualResult, expectedResult);
		
		Thread.sleep(7000);

  }
	
	
	@Test(enabled=false,priority = 20,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter Valid large text in First Name with lower case in Last Name Fileds")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter Valid large text in First Name with lower case in Last Name Fileds")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter Valid large text in First Name with lower case in Last Name Fileds")
	public void validateEnterValidLargeFirstName() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//enter first and last name
		Thread.sleep(4000);
		customerProfilePage.validatesEnterFirstandLastName(prop.getProperty("ValidLargetextFirstName"),prop.getProperty("ValidUpperLowerCaseLastName"));
		
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		String expectedResult = "Customer updated successfully!";
		String ActualResult= "Customer updated successfully!";
		Assert.assertEquals(ActualResult, expectedResult);
		
		Thread.sleep(7000);	
		
  }
	
	@Test(enabled=false,priority = 21,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter Invalid Lower case with numeric value in Name Filed and Enter valid lower case in Last Name Fileds")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter Invalid Lower case with numeric value in Name Filed and Enter valid lower case in Last Name Fileds")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter Invalid Lower case with numeric value in Name Filed and Enter valid lower case in Last Name Fileds")
	public void validateEnterInvalidLowercasewithnumericvalueFirstName() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//enter first and last name
		Thread.sleep(4000);
		customerProfilePage.validatesEnterFirstandLastName(prop.getProperty("InValidNumericValueFirstName"),prop.getProperty("ValidUpperLowerCaseLastName"));
		
		
		String expectedResult = customerProfilePage.validateValidationMessageofFirstName();
		String ActualResult= "Required";
		Assert.assertEquals(ActualResult, expectedResult,"Validation message not displayed");		
		
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		
		String expectedResult1 = "Update button disabled";
		String ActualResult1= "Update button disabled";
		Assert.assertEquals(ActualResult1, expectedResult1);
		
		Thread.sleep(7000);	
		
  }
	
	@Test(enabled=false,priority = 22,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter blank Invalid Name Filed and Enter valid lower case in Last Name Fileds")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter blank Invalid Name Filed and Enter valid lower case in Last Name Fileds")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter blank Invalid Name Filed and Enter valid lower case in Last Name Fileds")
	public void validateEnterInvalidBlankTextInFirstNameField() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//enter first and last name
		Thread.sleep(4000);
		customerProfilePage.validatesEnterFirstandLastName(prop.getProperty("InValidBlankFirstName"),prop.getProperty("ValidUpperLowerCaseLastName"));		
		
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		
		String expectedResult1 = "Update button disabled";
		String ActualResult1= "Update button disabled";
		Assert.assertEquals(ActualResult1, expectedResult1);
		
		Thread.sleep(7000);	
		
  }
	
	@Test(enabled=false,priority = 23,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter valid Lower case with in Name Filed and Enter invalid lower case with numeric value in Last Name Fileds")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter valid Lower case with in Name Filed and Enter invalid lower case with numeric value in Last Name Fileds")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter valid Lower case with in Name Filed and Enter invalid lower case with numeric value in Last Name Fileds")
	public void validateEnterInvalidLowercasewithnumericvaluelastName() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//enter first and last name
		Thread.sleep(4000);
		customerProfilePage.validatesEnterFirstandLastName(prop.getProperty("ValidLowerCaseFirstName"),prop.getProperty("InValidNumericValuelastName"));
		
		
		String expectedResult ="Enter alphabets only";
		String ActualResult= "Enter alphabets only";
		Assert.assertEquals(ActualResult, expectedResult,"Validation message not displayed");		
		
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		
		String expectedResult1 = "Update button disabled";
		String ActualResult1= "Update button disabled";
		Assert.assertEquals(ActualResult1, expectedResult1);
		
		Thread.sleep(7000);	
		
  }
	
	@Test(enabled=false,priority = 24,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter valid email Id and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter valid email Id and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter valid email Id and finally click on update button")
	public void validateEntervalidEmailId() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//enter valid email id
		Thread.sleep(4000);
		customerProfilePage.validatesSearchWithEmail(prop.getProperty("ValidEmailId"));
		
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		
		String expectedResult1 = "Update button disabled";
		String ActualResult1= "Update button disabled";
		Assert.assertEquals(ActualResult1, expectedResult1);
		
		Thread.sleep(7000);	
		
  }
	
	@Test(enabled=false,priority = 25,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from how you came to known and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from how you came to known and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from how you came to known and finally click on update button")
	public void validatebySelectingrandomlyanyDropdownlistfromHowyoucameToKnownFiled() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//select ranmdoly any dropdown list from how you came to known filed
		Thread.sleep(4000);
		customerProfilePage.validateselectinganyRandomHowyouCameToKnown();
		
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		
		String expectedResult1 = "Update button disabled";
		String ActualResult1= "Update button disabled";
		Assert.assertEquals(ActualResult1, expectedResult1);
		
		Thread.sleep(7000);	
		
  }
	

	@Test(enabled=false,priority = 26,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from Ring bell and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from ring bell and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from ring bell and finally click on update button")
	public void validatebySelectingrandomlyanyDropdownlistfromRingBellFiled() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//select ranmdoly any dropdown list from ring bell filed
		Thread.sleep(4000);
		customerProfilePage.validateselectinganyRandomRingBell();
		
		Thread.sleep(2000);
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		String expectedResult = "Customer updated successfully!";
		String ActualResult= "Customer updated successfully!";
		Assert.assertEquals(ActualResult, expectedResult);
		
		Thread.sleep(7000);	
		
  }
	

	@Test(enabled=false,priority = 27,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from stop delivery sms and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from stop delivery sms and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list stop delivery sms and finally click on update button")
	public void validatebySelectingrandomlyanyDropdownlistfromStopDeliverysmsFiled() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//select ranmdoly any dropdown list from stop delivery sms filed
		Thread.sleep(4000);
		customerProfilePage.validateselectinganyStopdeliverysmslist();
		
		Thread.sleep(2000);
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		String expectedResult = "Customer updated successfully!";
		String ActualResult= "Customer updated successfully!";
		Assert.assertEquals(ActualResult, expectedResult);
		
		Thread.sleep(7000);	
		
  }
	

	@Test(enabled=false,priority = 28,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from OBD Calls and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from OBD Calls and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list OBD Calls and finally click on update button")
	public void validatebySelectingrandomlyanyDropdownlistfromOBDCallsFiled() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//select ranmdoly any dropdown list from OBD Calls filed
		Thread.sleep(4000);
		customerProfilePage.validateselectinganyOBDCallslist();
		
		Thread.sleep(2000);
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		
		String expectedResult = "Customer updated successfully!";
		String ActualResult= "Customer updated successfully!";
		Assert.assertEquals(ActualResult, expectedResult);
		
		Thread.sleep(7000);	
		
  }
	
	@Test(enabled=false,priority = 29,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from Bill Cycles and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from Bill Cycles and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list Bill Cycles and finally click on update button")
	public void validatebySelectingrandomlyanyDropdownlistfromBillCyclesFiled() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//select ranmdoly any dropdown list from Bill Cycles filed
		Thread.sleep(4000);
		customerProfilePage.validateselectinganyBillCycleslist();
		
		Thread.sleep(2000);
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		String expectedResult = "Customer updated successfully!";
		String ActualResult= "Customer updated successfully!";
		Assert.assertEquals(ActualResult, expectedResult);
		
		Thread.sleep(7000);	
		
  }
	
	@Test(enabled=false,priority = 30,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from Positive wallet and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from Positive wallet and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from Positive wallet and finally click on update button")
	public void validatebySelectingrandomlyanyDropdownlistfromPositiveWalletFiled() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//select ranmdoly any dropdown list from positive wallet filed
		Thread.sleep(4000);
		customerProfilePage.validateselectinganyPositiveWalletlist();
		
		Thread.sleep(2000);
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		String expectedResult = "Customer updated successfully!";
		String ActualResult= "Customer updated successfully!";
		Assert.assertEquals(ActualResult, expectedResult);
		
		Thread.sleep(7000);	
		
  }
	
	@Test(enabled=false,priority = 31,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter positive wallet remark and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter positive wallet remark and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter positive wallet remark and finally click on update button")
	public void validatebyEnterPositiveWalletRemark() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//Enter positive wallet remark
		Thread.sleep(4000);
		customerProfilePage.validatesEnterPositiveWalletRemark(prop.getProperty("positivewalletRemark"));
		
		Thread.sleep(2000);
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		String expectedResult = "Customer updated successfully!";
		String ActualResult= "Customer updated successfully!";
		Assert.assertEquals(ActualResult, expectedResult);
		
		Thread.sleep(7000);	
		
  }
	
	@Test(enabled=false,priority = 32,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter Preference remark and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter Preference remark and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and Enter Preference remark and finally click on update button")
	public void validatebyEnterPreferenceRemark() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//Enter preference remark
		Thread.sleep(4000);
		customerProfilePage.validatesEnterPreferenceRemark(prop.getProperty("preferenceremark"));
		
		Thread.sleep(2000);
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		String expectedResult = "Customer updated successfully!";
		String ActualResult= "Customer updated successfully!";
		Assert.assertEquals(ActualResult, expectedResult);
		
		Thread.sleep(7000);	
		
  }
	
	@Test(enabled=false,priority = 33,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from promotional Calls and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from promotional Calls and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from promotional Calls and finally click on update button")
	public void validatebySelectingrandomlyanyDropdownlistfromPromotionalCallsFiled() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//select randomly any promotional calls from drpdown list
		Thread.sleep(4000);
		customerProfilePage.validateselectinganyPromotionalCallslist();
		
		Thread.sleep(2000);
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		Thread.sleep(7000);

		String ActualResult = driver.findElement(By.xpath("/html/body/div[2]/span[2]")).getText();

		System.out.println("text message: " + ActualResult);

		if (ActualResult == "Customer updated successfully!") {

			System.out.println("Popup display With valid message");
			Assert.assertEquals(ActualResult, "Customer updated successfully!");

		}

		Assert.assertEquals(ActualResult, "Customer updated successfully!", "Popup display With invalid message");

	}
		
	
	@Test(enabled=false,priority = 34,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from promotional sms and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from promotional sms and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from promotional sms and finally click on update button")
	public void validatebySelectingrandomlyanyDropdownlistfromPromotionalSMSFiled() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//select randomly any promotional sms from drpdown list
		Thread.sleep(4000);
		customerProfilePage.validateselectinganyPromotionalsmslist();
		
		Thread.sleep(2000);
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		Thread.sleep(7000);

		String ActualResult = driver.findElement(By.xpath("/html/body/div[2]/span[2]")).getText();

		System.out.println("text message: " + ActualResult);

		if (ActualResult == "Customer updated successfully!") {

			System.out.println("Popup display With valid message");
			Assert.assertEquals(ActualResult, "Customer updated successfully!");

		}

		Assert.assertEquals(ActualResult, "Customer updated successfully!", "Popup display With invalid message");

	}
		
	
	@Test(enabled=false,priority = 35,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from preference timeslot and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from preference timeslot  and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and selecting randomly any dropdown list from preference timeslot  and finally click on update button")
	public void validatebySelectingrandomlyanyDropdownlistfromPreferenceTimeSlotFiled() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//select randomly any prefrence timeslot from drpdown list
		Thread.sleep(4000);
		customerProfilePage.validateselectinganyPreferenceTimeSlotlist();
		
		Thread.sleep(2000);
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		Thread.sleep(7000);

		String ActualResult = driver.findElement(By.xpath("/html/body/div[2]/span[2]")).getText();

		System.out.println("text message: " + ActualResult);

		if (ActualResult == "Customer updated successfully!") {

			System.out.println("Popup display With valid message");
			Assert.assertEquals(ActualResult, "Customer updated successfully!");

		}

		Assert.assertEquals(ActualResult, "Customer updated successfully!", "Popup display With invalid message");

	}
		
	
	
	@Test(enabled=false,priority = 36,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter valid address line1 and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter valid address line1 and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter valid address line1 and finally click on update button")
	public void validateByEnterValidAddressLine1() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//Enter address line 1
		Thread.sleep(4000);
		customerProfilePage.validatesEnterAddressLine1(prop.getProperty("ValidAddressLine1"));
		
		Thread.sleep(2000);
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		String expectedResult1 = "Update button disabled";
		String ActualResult1= "Update button disabled";
		Assert.assertEquals(ActualResult1, expectedResult1);
		
		Thread.sleep(7000);	
		
  }

	
	@Test(enabled=false,priority = 37,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter large valid address line1 and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter large valid address line1 and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter large valid address line1 and finally click on update button")
	public void validateByEnterLargeValidAddressLine1() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//Enter address line 1
		Thread.sleep(4000);
		customerProfilePage.validatesEnterAddressLine1(prop.getProperty("LargeValidAddressLine1"));
		
		Thread.sleep(2000);
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		String expectedResult1 = "Update button disabled";
		String ActualResult1= "Update button disabled";
		Assert.assertEquals(ActualResult1, expectedResult1);
		
		Thread.sleep(7000);	
		
  }	
	
	
	@Test(enabled=false,priority = 38,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter blank text invalid address line1 and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter blank text invalid address line1 and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter blank text invalid address line1 and finally click on update button")
	public void validateByEnterinValidAddressLine1() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//Enter address line 1
		Thread.sleep(4000);
		customerProfilePage.validatesEnterAddressLine1(prop.getProperty("invalidAddressLine1"));
		
		Thread.sleep(2000);
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		String expectedResult1 = "Update button disabled";
		String ActualResult1= "Update button disabled";
		Assert.assertEquals(ActualResult1, expectedResult1);
		
		Thread.sleep(7000);	
		
  }	
	
	
	@Test(enabled=false,priority = 39,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter valid address line2 and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter valid address line2 and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter valid address line2 and finally click on update button")
	public void validateByEnterValidAddressLine2() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//Enter address line 1
		Thread.sleep(4000);
		customerProfilePage.validatesEnterAddressLine2(prop.getProperty("ValidAddressLine2"));
		
		Thread.sleep(2000);
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		String expectedResult1 = "Update button disabled";
		String ActualResult1= "Update button disabled";
		Assert.assertEquals(ActualResult1, expectedResult1);
		
		Thread.sleep(7000);	
		
  }
	
	@Test(enabled=false,priority = 40,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter valid address line3 and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter valid address line3 and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter valid address line3 and finally click on update button")
	public void validateByEnterValidAddressLine3() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//Enter address line 1
		Thread.sleep(4000);
		customerProfilePage.validatesEnterAddressLine3(prop.getProperty("ValidAddressLine3"));
		
		Thread.sleep(2000);
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		String expectedResult1 = "Update button disabled";
		String ActualResult1= "Update button disabled";
		Assert.assertEquals(ActualResult1, expectedResult1);
		
		Thread.sleep(7000);	
		
  }
	
	@Test(enabled=false,priority = 41,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and updating city and locality and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and updating city and locality and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and updating city and locality and finally click on update button")
	public void validateBySelectCityandLocality() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//Selecting city and locality
		Thread.sleep(4000);
		customerProfilePage.validateselectinganyCityLocality();
		
		Thread.sleep(2000);
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		String expectedResult1 = "Update button disabled";
		String ActualResult1= "Update button disabled";
		Assert.assertEquals(ActualResult1, expectedResult1);
		
		Thread.sleep(7000);	
		
  }
	
	
	@Test(enabled=false,priority = 42,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter valid State and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter valid state and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter valid state and finally click on update button")
	public void validateByEnterValidState() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//Enter Valid State
		Thread.sleep(4000);
		customerProfilePage.validatesEnterState(prop.getProperty("ValidState"));
		
		Thread.sleep(2000);
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		Thread.sleep(7000);

		String ActualResult = driver.findElement(By.xpath("/html/body/div[2]/span[2]")).getText();

		System.out.println("text message: " + ActualResult);

		if (ActualResult == "Customer updated successfully!") {

			System.out.println("Popup display With valid message");
			Assert.assertEquals(ActualResult, "Customer updated successfully!");

		}

		Assert.assertEquals(ActualResult, "Customer updated successfully!", "Popup display With invalid message");

	}
		
	
	@Test(enabled=false,priority = 43,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter valid pincode and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter valid pincode and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and enter valid pincode and finally click on update button")
	public void validateByEnterValidPinCode() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();
		
		//click on edit profile button
		Thread.sleep(4000);	
		customerProfilePage.ClickOnEditProfileButton();
		
		//Enter Valid pincode
		Thread.sleep(4000);
		customerProfilePage.validatesEnterPinCode(prop.getProperty("ValidPinCode"));
		
		Thread.sleep(2000);
	    //click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();
		
		Thread.sleep(2000);
		//click on update button
		customerProfilePage.ClickOnUpdateButton();
		
		Thread.sleep(7000);

		String ActualResult = driver.findElement(By.xpath("/html/body/div[2]/span[2]")).getText();

		System.out.println("text message: " + ActualResult);

		if (ActualResult == "Customer updated successfully!") {

			System.out.println("Popup display With valid message");
			Assert.assertEquals(ActualResult, "Customer updated successfully!");

		}

		Assert.assertEquals(ActualResult, "Customer updated successfully!", "Popup display With invalid message");

	}
		

	@Test(priority = 44, description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and move to need sepaerate billing address section and enter address line 1 filed  and also select city only and finally click on update button")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and move to need sepaerate billing address section and enter address line 1 filed  and also select city only and finally click on update button")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and click on edit profile button and move to need sepaerate billing address section and enter address line 1 filed  and also select city only and finally click on update button")
	public void validateSeperateBillingAddressSectionAndEnterValidAddressLine1() throws Exception {

		// click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();

		// click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();

		Thread.sleep(5000);

		// Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));

		Thread.sleep(2000);
		// click on search button
		customerProfilePage.clickOnSearchButton();

		Thread.sleep(4000);
		// click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();

		// click on edit profile button
		Thread.sleep(4000);
		customerProfilePage.ClickOnEditProfileButton();

		Thread.sleep(2000);
		// click on NEED A SEPERATE BILLING ADDRESS? radio button
		customerProfilePage.ClickOnNeedaSeparateBillingAddressButton();

		// Enter address line 1
		Thread.sleep(4000);
		customerProfilePage.validatesNeedToeSeparateBillingAddressSectionEnterAddressLine1(
				prop.getProperty("SeperateValidAddressLine1"));

		Thread.sleep(2000);
		// selecting city
		customerProfilePage.validateselectinganyCityOnly();

		Thread.sleep(2000);
		// click on update button
		customerProfilePage.ClickOnUpdateButton();

		Thread.sleep(5000);

		String ActualResult = driver.findElement(By.xpath("/html/body/div[3]/span[2]")).getText();

		System.out.println("text message: " + ActualResult);

		if (ActualResult == "Customer updated successfully!") {

			System.out.println("Popup display With valid message");
			Assert.assertEquals("Customer updated successfully!", "Customer updated successfully!");

		}

		Assert.assertEquals(ActualResult, "Customer updated successfully!", "Popup display With invalid message");

	}
	
	
	
	@Test(enabled=false,priority = 17,description = "Verify that Search with valid Phone Number for customer and clicking it and finally customer profile will open and clicking one by one filed.")
	@Severity(SeverityLevel.BLOCKER)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate Search with valid Phone Number for customer and clicking it and finally customer profile will open and clicking one by one filed.")
	@Story("Story Name : To check Search with valid Phone Number for customer and clicking it and finally customer profile will open and clicking one by one filed.")
	public void validateselectingCustomerandClickingOnebyOne() throws Exception {
		
		//click on customer section link
		customerProfilePage.clickOnCustomerSectionButton();
		
		//click on customer profile link
		customerProfilePage.clickOnCustomerProfileButton();
		
		Thread.sleep(5000);
		
		//Enter phone number
		customerProfilePage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(2000);
		//click on search button
		customerProfilePage.clickOnSearchButton();
		
		Thread.sleep(4000);
		//click on sorted customer
		customerProfilePage.clickOnlistOfCustomerData();

		Thread.sleep(4000);
		//click one by one filed
		customerProfilePage.clickonOneByOneFiled();
	
		Thread.sleep(6000);
		
	}

	
	
	@AfterMethod(description = "Browser Tear Down")
	public void End() {
		
		driver.quit();
		driver=null;

	}

	
}
