package com.new_cms_admin.qa.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.new_cms_admin.qa.base.TestBase;


public class LoginCredentialsUtils extends TestBase {

	
	
    public static String TESTDATA_SHEET_PATH="/home/dell/eclipse-workspace/new-cms-admin"
    		+ "/src/main/resources/excel/CustomerDetails.xlsx";
	
	
	
	static Workbook book;
	static Sheet sheet;
	
	public static Object[][] getTestData(String sheetNames){
		
		FileInputStream file=null;
		
		try {
			file=new FileInputStream(TESTDATA_SHEET_PATH);
		}
		catch(FileNotFoundException e){
			e.printStackTrace();
			
		}
		try {
			book=WorkbookFactory.create(file);
//		}catch(InvalidFormatException e) {
//			e.printStackTrace();
		}catch(IOException e) {
			e.printStackTrace();
			
		}
		sheet=book.getSheet(sheetNames);
		
		Object[][] data= new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
		

		for(int i=0;i<sheet.getLastRowNum();i++) {
			for(int k=0;k<sheet.getRow(0).getLastCellNum();k++) {
				//System.out.println(i);
				//System.out.println(sheet.getFirstRowNum()+);
				if(sheet.getRow(i +1).getCell(k)!=null) {
				data[i][k]=sheet.getRow(i +1).getCell(k).toString();
				}else {
					data[i][k]="";
				}
				
			}
		}
		return data;

	}
}



