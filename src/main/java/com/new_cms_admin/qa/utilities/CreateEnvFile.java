package com.new_cms_admin.qa.utilities;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import com.new_cms_admin.qa.base.TestBase;

public class CreateEnvFile extends TestBase{
	
	public void createFile(String platForm, String buildUrl, String testUrl) {
		
		Properties properties = new Properties();
		
		properties.setProperty("Platform", platForm);
		properties.setProperty("BuildUrl", buildUrl);
		properties.setProperty("TestUrl", testUrl);
		FileWriter writer=null;
		
		try {
			
			writer=new FileWriter("/home/dell/eclipse-workspace/new-cms-admin/allure-results/environment.properties");
			properties.store(writer, "Jiri Pinkas");
			
		} catch(IOException ex) {
			
			ex.printStackTrace();
			
		} finally {
			
			if(writer !=null) {
				
				try {
					
					writer.close();
					
				} catch(IOException ex) {
					
					ex.printStackTrace();
					
				}
				
			}
			
		}
		
		
	}


}
