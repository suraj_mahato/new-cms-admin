package com.new_cms_admin.qa.pages;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.new_cms_admin.qa.base.TestBase;

import io.qameta.allure.Step;

public class PlaceOrderPage extends TestBase {

	// Page Factory OR:

	@FindBy(xpath = "//div[contains(text(),'Search Customer')]")
	WebElement SearchcustomerFiledXpath;

	@FindBy(xpath = "//input[@placeholder='Phone']")
	WebElement phoneFiledXpath;

	@FindBy(xpath = "/html/body/app-root/app-admin-layout/div/div/app-search-customer/div/div/div/div/form/div/button")
	WebElement SearchButtonXpath;

	@FindBy(xpath = "//td[@class='mat-cell cdk-column-phoneNo mat-column-phoneNo ng-star-inserted']")
	WebElement customerListXpath;
	
	@FindBy(xpath="//a[normalize-space()='Place Order']")
	WebElement placeOrderButtonXpath;
	
	@FindBy(xpath="//button[normalize-space()='Place Order']")
	WebElement placeOrderPageButtonXpath;
	
	@FindBy(xpath="//span[normalize-space()='Place Order']")
	WebElement popupPlaceOrderButtonXpath;
	
	@FindBy(xpath="//div[normalize-space()='Milk']")
	WebElement milklinkXpath;
	
	@FindBy(xpath="/html/body/app-root/app-admin-layout/div/div/app-create-order/div/div/div/div/div[2]/form/div/div[3]/div[2]/table/tbody/tr[3]/td[3]/mat-form-field/div/div[1]/div[3]")
	WebElement CowMilkQtyFiledXpath;
	
	@FindBy(xpath="/html/body/div[4]/span[2]")
	WebElement validateValidationMessageXpath;
	
	@FindBy(xpath="//*[@id=\"mat-select-91\"]")
	WebElement subscriptionSectionXpath;
	
	@FindBy(xpath="//span[normalize-space()='Recurring']")
	WebElement recurringXpath;
	
	@FindBy(xpath="//span[normalize-space()='Alternate']")
	WebElement alternateXpath;
	
	@FindBy(xpath="//span[normalize-space()='One Time']")
	WebElement onTimeXpath;
	
	

	// Initializing the page objects
	public PlaceOrderPage() {

		PageFactory.initElements(driver, this);
	}
	
	

	// Actions:

	

	@Step("click on cutsomer section button")
	public void clickOnSearchCustomerSection() throws Exception {

		SearchcustomerFiledXpath.click();

	}

	@Step("Enter Phone Number")
	public void validatesSearchWithphoneNumber(String phone) throws Exception {

		// creating Actions class
		Actions a = new Actions(driver);

		// click on phone section
		phoneFiledXpath.click();

		// Enter a text
		a.sendKeys(phone);

		a.perform();

	}
	
	@Step("Enter the qty")
	public void Enterqty(String qty) throws Exception {

		// creating Actions class
		Actions a = new Actions(driver);

		// Enter a qty
		CowMilkQtyFiledXpath.click();

		// Enter a text
		a.sendKeys(qty);

		a.perform();

	}

	@Step("click on Search button")
	public void clickOnSearchButton() throws Exception {

		// check that search button is enabled then click it
		if (driver.findElement(By.xpath("/html/body/app-root/app-admin-layout/div"
				+ "/div/app-search-customer/div/div/div/div/form/div/button")).isEnabled()) {

			System.out.println(" Button is enabled");

			// click on search button
			SearchButtonXpath.click();

		}

		else {

			System.out.println("Button is disbaled");

		}

	}

	@Step("click on list of customer")
	public void clickOnlistOfCustomerData() throws Exception {

		// Sorted data is display or not

		if (!driver.findElements(
						By.xpath("//td[@class='mat-cell cdk-column-phoneNo mat-column-phoneNo ng-star-inserted']")).isEmpty()) {

			System.out.println("list of data is display");

			customerListXpath.click();

		} else {

			System.out.println("list of data is not display");

		}

	}
	
	
	@Step("click on place an order button")
	public void clickonPlaceOrderButton() throws Exception {
		

		//click on place an order button
		placeOrderButtonXpath.click();
		
        Set<String> allWindows=driver.getWindowHandles();
        
        System.out.println("ALl window is is: "+allWindows);
		
		int count=allWindows.size();
		
		System.out.println("Total window count: "+count);
		
		
		
	}
	

	
	@Step("Select milk product and updtae qty=1")
	public void SelectMilkProduct() throws Exception {
		
	   Set<String> allWindows=driver.getWindowHandles();
		
		for(String child: allWindows)
		
		{
			String mainWindowHandle = driver.getWindowHandle();
			
			if(!mainWindowHandle.equalsIgnoreCase(child))
		
			{
				
				driver.switchTo().window(child);
				
				//click on milk link
			    milklinkXpath.click();
			    
			    
					
				
			}
			
		}
		
		
	}
	
	
	@Step("Select a particular subscription from dropdown list")
	public void selectaParticularDrownListSubs(String subs) throws Exception {
		
		
		
	}
	
	
	
	
	@Step("Click on place an order button in create order page")
	public void ClickOnPlaceOrderButton() throws Exception {
		
		//click on place an order button
		placeOrderPageButtonXpath.click();
		
	}
	
	
	@Step("Click on place an order button in create order page")
	public void ClickOnPopUpPlaceOrderButton() throws Exception {
		
		//click on place an order button
		popupPlaceOrderButtonXpath.click();
		
	}
	
	@Step("Validation message of successfully order place ")
	public String validationmessageofSuccessfullyOrderPlace() throws Exception {

		return validateValidationMessageXpath.getText();

	}

	
	
	
	
	
	

}
