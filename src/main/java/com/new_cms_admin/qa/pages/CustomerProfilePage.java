package com.new_cms_admin.qa.pages;

import java.util.List;
import java.util.Random;

import javax.swing.plaf.basic.BasicBorders.RadioButtonBorder;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.new_cms_admin.qa.base.TestBase;

import io.qameta.allure.Step;

public class CustomerProfilePage extends TestBase {

	// Page Factory OR:

	@FindBy(xpath = "/html/body/app-root/app-admin-layout/div/app-new-sidenav/div/div[1]/ng-material-multilevel-menu/div/mat-list/ng-list-item[6]/div/mat-list-item/div/a/div[1]/span")
	WebElement customerFiledXpath;

	@FindBy(xpath = "/html/body/app-root/app-admin-layout/div/app-sidebar/div/div[1]/ul/li[6]/li[3]/a/p")
	WebElement customerProfileFiledXpath;

	@FindBy(xpath = "//div[@class='cd-common-header']")
	WebElement customerSearchPageXpath;

	@FindBy(xpath = "//input[@placeholder='Phone']")
	WebElement phoneFiledXpath;

	@FindBy(xpath = "//input[@placeholder='Name']")
	WebElement nameFiledXpath;

	@FindBy(xpath = "//input[@placeholder='Address']")
	WebElement addressFiledXpath;

	@FindBy(xpath = "//input[@placeholder='Email']")
	WebElement emailFiledXpath;

	@FindBy(xpath = "//input[@placeholder='Customer id']")
	WebElement customerIdFiledXpath;

	@FindBy(xpath = "//input[@placeholder='Select route']")
	WebElement selectRouteXpath;

	@FindBy(xpath = "//input[@placeholder='Select area']")
	WebElement selectAreaXpath;

	@FindBy(xpath = "/html/body/app-root/app-admin-layout/div/div/app-search-customer/div/div/div/div/form/div/button")
	WebElement SearchButtonXpath;

	@FindBy(xpath = "//div[@class='d-flex justify-content-center ng-star-inserted']")
	WebElement validateMessageXpath;
	
	@FindBy(xpath="//td[@class='mat-cell cdk-column-phoneNo mat-column-phoneNo ng-star-inserted']")
	WebElement customerListXpath;
	
	@FindBy(xpath="//mat-panel-title[@class='text-primary justify-content-center mat-expansion-panel-header-title']")
	WebElement clickToViewMoreInfoLinkXpath;
	
	@FindBy(xpath="//div[@class='few-details']//div//a[@type='button']")
	WebElement editProfileButtonXpath;
	
	@FindBy(xpath = "//input[@placeholder='First name']")
	WebElement FirstNameFiledXpath;
	
	@FindBy(xpath = "//input[@placeholder='Last name']")
	WebElement LastNameFiledXpath;
	
	@FindBy(xpath = "//input[@placeholder='email']")
	WebElement emailsFiledXpath;
	
	@FindBy(xpath = "//input[@placeholder='How you came to know?']")
	WebElement howyoucameFiledXpath;
	
	@FindBy(xpath = "//*[@id='mat-select-0']/div/div[2]")
	WebElement ringBellFiledXpath;	
	
	@FindBy(xpath = "//*[@id='mat-select-1']/div/div[2]")
	WebElement stopDeliverySmsFiledXpath;		
	
	@FindBy(xpath = "//*[@id='mat-select-2']/div/div[2]")
	WebElement ObdCallFiledXpath;	
	
	@FindBy(xpath = "//*[@id='mat-select-3']/div/div[2]")
	WebElement BillCycleFiledXpath;	
	
	@FindBy(xpath = "//*[@id='mat-select-4']/div/div[2]")
	WebElement PostiveWalletFiledXpath;	
	
	@FindBy(xpath = "/html/body/app-root/app-admin-layout/div/div/app-edit-customer/div/div/div/div/form/div[16]/mat-form-field/div/div[1]/div/mat-select/div/div[2]")
	WebElement PromotionalCallFiledXpath;		
	
	@FindBy(xpath = "/html/body/app-root/app-admin-layout/div/div/app-edit-customer/div/div/div/div/form/div[17]/mat-form-field/div/div[1]/div/mat-select/div/div[2]")
	WebElement PromotionalsmsFiledXpath;		
	
	@FindBy(xpath = "/html/body/app-root/app-admin-layout/div/div/app-edit-customer/div/div/div/div/form/div[18]/mat-form-field/div/div[1]/div/mat-select/div/div[2]")
	WebElement PreferredTimeSlotFiledXpath;		
	
	@FindBy(xpath = "//input[@placeholder='Positive Wallet Remark']")
	WebElement postiveWalletRemarkFiledXpath;	
	
	@FindBy(xpath = "//input[@placeholder='Preference Remark']")
	WebElement preferenceRemarkFiledXpath;	
	
	@FindBy(xpath = "//input[@placeholder='Address line 1']")
	WebElement addressLine1FiledXpath;	
	
	@FindBy(xpath = "//input[@placeholder='Address line 2']")
	WebElement addressLine2FiledXpath;	
	
	@FindBy(xpath = "//input[@placeholder='Address line 3']")
	WebElement addressLine3FiledXpath;	
	
	@FindBy(xpath = "//input[@placeholder='Select locality']")
	WebElement selectLocalityFiledXpath;	
	
	@FindBy(xpath = "//input[@placeholder='Select city']")
	WebElement selectCityFiledXpath;	
	
	@FindBy(xpath = "//input[@placeholder='State']")
	WebElement stateFiledXpath;	
	
	@FindBy(xpath = "//input[@placeholder='pinCode']")
	WebElement pincodeFiledXpath;	
	
	@FindBy(xpath = "//div[@class='mat-checkbox-inner-container']")
	WebElement needSeparateRadioButtonXpath;	
	
	@FindBy(xpath = "//mat-form-field[@class='mat-form-field ng-tns-c9-45 mat-primary mat-form-field-type-mat-input mat-form-field-appearance-legacy mat-form-field-can-float mat-form-field-has-label mat-form-field-hide-placeholder ng-untouched ng-pristine ng-invalid']//div[@class='mat-form-field-infix']")
	WebElement SeparateaddressLine1FiledXpath;	
	
	@FindBy(xpath = "//mat-form-field[@class='mat-form-field ng-tns-c9-46 mat-primary mat-form-field-type-mat-input mat-form-field-appearance-legacy mat-form-field-can-float mat-form-field-has-label mat-form-field-hide-placeholder ng-untouched ng-pristine ng-valid']//div[@class='mat-form-field-infix']")
	WebElement SeparateaddressLine2FiledXpath;	
	
	@FindBy(xpath = "//mat-form-field[@class='mat-form-field ng-tns-c9-47 mat-primary mat-form-field-type-mat-input mat-form-field-appearance-legacy mat-form-field-can-float mat-form-field-has-label mat-form-field-hide-placeholder ng-untouched ng-pristine ng-valid']//div[@class='mat-form-field-infix']")
	WebElement SeparateaddressLine3FiledXpath;	
	
	@FindBy(xpath = "/html[1]/body[1]/app-root[1]/app-admin-layout[1]/div[1]/div[1]/app-edit-customer[1]/div[1]/div[1]/div[1]/div[1]/div[4]/form[1]/div[4]/mat-form-field[1]/div[1]/div[1]/div[1]/input[1]")
	WebElement SeparateselectCityFiledXpath;	
	
	@FindBy(xpath = "/html[1]/body[1]/app-root[1]/app-admin-layout[1]/div[1]/div[1]/app-edit-customer[1]/div[1]/div[1]/div[1]/div[1]/div[4]/form[1]/div[5]/mat-form-field[1]/div[1]/div[1]/div[1]/input[1]")
	WebElement SeparatestateFiledXpath;	
	
	@FindBy(xpath = " /html[1]/body[1]/app-root[1]/app-admin-layout[1]/div[1]/div[1]/app-edit-customer[1]/div[1]/div[1]/div[1]/div[1]/div[4]/form[1]/div[6]/mat-form-field[1]/div[1]/div[1]/div[1]/input[1]")
	WebElement SeparatepincodeFiledXpath;	
	
	@FindBy(xpath="//span[normalize-space()='Update']")
	WebElement updatButtonXpath;
	
	@FindBy(xpath="//*[@class='mat-error']")
	WebElement validateFirstNameTextXpath;
	
	@FindBy(xpath="/html/body/app-root/app-admin-layout/div/div/app-edit-customer/div/div/div/div/form/div[2]/mat-form-field/div/div[3]/div/mat-error")
	WebElement validatelastNameTextXpath;
	
	
	
	
	// Initializing the page objects
	public CustomerProfilePage() {

		PageFactory.initElements(driver, this);
	}

	// Actions:

	@Step("click on cutsomer section button")
	public void clickOnCustomerSectionButton() throws Exception {

		customerFiledXpath.click();

	}

	@Step("click on cutsomer profile button")
	public void clickOnCustomerProfileButton() throws Exception {

		customerProfileFiledXpath.click();

	}

	@Step("Validate Customer search Page ")
	public String validateCustomerSearchPage() throws Exception {

		return customerSearchPageXpath.getText();

	}

	@Step("Validate that validation message ")
	public String validateValidationMessage() throws Exception {

		return validateMessageXpath.getText();

	}
	
	@Step("Validate that validation message of first name filed ")
	public String validateValidationMessageofFirstName() throws Exception {

		return validateFirstNameTextXpath.getText();

	}

	@Step("Validate that validation message of last name filed ")
	public String validateValidationMessageoflastName() throws Exception {

		return validatelastNameTextXpath.getText();

	}
	

	@Step("click on edit icon")
	public void ClickOnEditProfileButton() throws Exception {
		
		editProfileButtonXpath.click();
		
	}

	@Step("click on Update Button")
	public void ClickOnUpdateButton() throws Exception {		

		// check that search button is enabled then click it
		if (driver.findElement(By.xpath("//span[normalize-space()='Update']")).isEnabled()) {

			System.out.println(" Is display");

			//scroll Down logic
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			
			jse.executeScript("window.scrollBy(0,1000)");
			Thread.sleep(2000);
			
			// click on update button
			updatButtonXpath.click();

		}

		else {

			System.out.println("Not display");

		}

	}
		
	
	@Step("click on NEED A SEPERATE BILLING ADDRESS? radio Button")
	public void ClickOnNeedaSeparateBillingAddressButton() throws Exception {
		
	//	needSeparateRadioButtonXpath.click();
		
		if(!driver.findElement(By.xpath("//div[@class='mat-checkbox-inner-container']")).isSelected()) {
			
			System.out.println("Is Selected");
			
			//click on radio button if selected
			needSeparateRadioButtonXpath.click();
			
		} else {
			
			System.out.println("Not Selected");
			
		}
		
	}
	
	
	@Step("Enter Phone Number")
	public void validatesSearchWithphoneNumber(String phone) throws Exception {

		// creating Actions class
		Actions a = new Actions(driver);

		// click on phone section
		phoneFiledXpath.click();

		// Enter a text
		a.sendKeys(phone);

		// click on enter keyword
		// a.sendKeys(Keys.ENTER);

		a.perform();

	}

	@Step("Enter Name")
	public void validatesSearchWithName(String name) throws Exception {

		// creating Actions class
		Actions a = new Actions(driver);

		// click on name section
		nameFiledXpath.click();

		// Enter a text
		a.sendKeys(name);

		a.perform();

	}

	@Step("Enter Address")
	public void validatesSearchWithAddress(String address) throws Exception {

		// creating Actions class
		Actions a = new Actions(driver);

		// click on address section
		addressFiledXpath.click();

		// Enter a text
		a.sendKeys(address);

		a.perform();

	}

	@Step("Enter Email")
	public void validatesSearchWithEmail(String email) throws Exception {

		// creating Actions class
		Actions a = new Actions(driver);

		// click on email section
		emailFiledXpath.click();
		
		//clear the text
		emailFiledXpath.clear();
		
		// click on email section
		emailFiledXpath.click();

		// Enter a text
		a.sendKeys(email);

		a.perform();

	}

	@Step("Enter Phone Number")
	public void validatesSearchWithCustomerId(String customerId) throws Exception {

		// creating Actions class
		Actions a = new Actions(driver);

		// click on phone section
		customerIdFiledXpath.click();

		// Enter a text
		a.sendKeys(customerId);

		// click on enter keyword
		// a.sendKeys(Keys.ENTER);

		a.perform();

	}
	
	@Step("Select randomly any how you came to known dropdown list")
	public void validateselectinganyRandomHowyouCameToKnown() throws Exception {

		howyoucameFiledXpath.click();

		// list methods
		List<WebElement> elements = driver.findElements(By.xpath("//*[@class='mat-option-text']"));

		System.out.println(elements.size());

		Random rnd = new Random();

		int rndInt = rnd.nextInt(elements.size());

		System.out.println("Random Number : " + rndInt);

		// select randomly any Route with any index wise
		elements.get(rndInt).click();

	}
	

	@Step("Select randomly any Route")
	public void validateselectinganyRandomRoute() throws Exception {

		selectRouteXpath.click();

		// list methods
		List<WebElement> elements = driver.findElements(By.xpath("/html/body/div/div/div/div/mat-option"));

		System.out.println(elements.size());

		Random rnd = new Random();

		int rndInt = rnd.nextInt(elements.size());

		System.out.println("Random Number : " + rndInt);

		// select randomly any Route with any index wise
		elements.get(rndInt).click();

	}
	

	@Step("Select randomly any ringBell from dropdown list")
	public void validateselectinganyRandomRingBell() throws Exception {
		
		//click on ring bell filed
    	ringBellFiledXpath.click();

		// list methods
		List<WebElement> elements = driver.findElements(By.xpath("//*[@class='mat-option-text']"));

		System.out.println(elements.size());

		Random rnd = new Random();

		int rndInt = rnd.nextInt(elements.size());

		System.out.println("Random Number : " + rndInt);

		// select randomly any Ring Bell with any index wise
		elements.get(rndInt).click();
	

	 
	}
	
	@Step("Select randomly any stop delivery sms from dropdown list")
	public void validateselectinganyStopdeliverysmslist() throws Exception {
		
		//click on  stop delivery sms filed
    	stopDeliverySmsFiledXpath.click();

		// list methods
		List<WebElement> elements = driver.findElements(By.xpath("//*[@class='mat-option-text']"));

		System.out.println(elements.size());

		Random rnd = new Random();

		int rndInt = rnd.nextInt(elements.size());

		System.out.println("Random Number : " + rndInt);

		// select randomly any stop delivery sms with any index wise
		elements.get(rndInt).click();
	
	}
	
	@Step("Select randomly any OBD Calls from dropdown list")
	public void validateselectinganyOBDCallslist() throws Exception {
		
		//click on  OBD Calls filed
    	ObdCallFiledXpath.click();

		// list methods
		List<WebElement> elements = driver.findElements(By.xpath("//*[@class='mat-option-text']"));

		System.out.println(elements.size());

		Random rnd = new Random();

		int rndInt = rnd.nextInt(elements.size());

		System.out.println("Random Number : " + rndInt);

		// select randomly any OBD Calls with any index wise
		elements.get(rndInt).click();
	
	}
	
	
	@Step("Select randomly any Bill Cycles from dropdown list")
	public void validateselectinganyBillCycleslist() throws Exception {
		
		//click on  bill cycles filed
    	BillCycleFiledXpath.click();

		// list methods
		List<WebElement> elements = driver.findElements(By.xpath("//*[@class='mat-option-text']"));

		System.out.println(elements.size());

		Random rnd = new Random();

		int rndInt = rnd.nextInt(elements.size());

		System.out.println("Random Number : " + rndInt);

		// select randomly any bill cycles with any index wise
		elements.get(rndInt).click();
	
	}
	
	@Step("Select randomly any positive wallet from dropdown list")
	public void validateselectinganyPositiveWalletlist() throws Exception {
		
		//click on  positive wallet filed
		PostiveWalletFiledXpath.click();

		// list methods
		List<WebElement> elements = driver.findElements(By.xpath("//*[@class='mat-option-text']"));

		System.out.println(elements.size());

		Random rnd = new Random();

		int rndInt = rnd.nextInt(elements.size());

		System.out.println("Random Number : " + rndInt);

		// select randomly any positive wallet with any index wise
		elements.get(rndInt).click();
	
	}
	
	
	@Step("Select randomly any Promotional calls from dropdown list")
	public void validateselectinganyPromotionalCallslist() throws Exception {
		
		//click on  promotional calls filed
		PromotionalCallFiledXpath.click();

		// list methods
		List<WebElement> elements = driver.findElements(By.xpath("//*[@class='mat-option-text']"));

		System.out.println(elements.size());

		Random rnd = new Random();

		int rndInt = rnd.nextInt(elements.size());

		System.out.println("Random Number : " + rndInt);

		// select randomly any promotional calls with any index wise
		elements.get(rndInt).click();
	
	}
	
	@Step("Select randomly any Promotional sms from dropdown list")
	public void validateselectinganyPromotionalsmslist() throws Exception {
		
		//click on  promotional sms filed
		PromotionalsmsFiledXpath.click();

		// list methods
		List<WebElement> elements = driver.findElements(By.xpath("//*[@class='mat-option-text']"));

		System.out.println(elements.size());

		Random rnd = new Random();

		int rndInt = rnd.nextInt(elements.size());

		System.out.println("Random Number : " + rndInt);

		// select randomly any promotional calls with any index wise
		elements.get(rndInt).click();
	
	}
	
	@Step("Select randomly any Prefrence timeslot from dropdown list")
	public void validateselectinganyPreferenceTimeSlotlist() throws Exception {
		
		//click on  Prefrence timeslots filed
		PreferredTimeSlotFiledXpath.click();

		// list methods
		List<WebElement> elements = driver.findElements(By.xpath("//*[@class='mat-option-text']"));

		System.out.println(elements.size());

		Random rnd = new Random();

		int rndInt = rnd.nextInt(elements.size());

		System.out.println("Random Number : " + rndInt);

		// select randomly any promotional calls with any index wise
		elements.get(rndInt).click();
	
	}
	
	@Step("Select randomly any Prefrence timeslot from dropdown list")
	public void validateselectinganyCityLocality() throws Exception {
		
		//click on city filed
		selectCityFiledXpath.click();

		// list methods
		List<WebElement> elements = driver.findElements(By.xpath("//*[@class='mat-option-text']"));

		System.out.println(elements.size());

		Random rnd = new Random();

		int rndInt = rnd.nextInt(elements.size());

		System.out.println("Random Number : " + rndInt);

		// select randomly any city with any index wise
		elements.get(rndInt).click();
		
		Thread.sleep(5000);
		
		//click on Locality filed
		selectLocalityFiledXpath.click();

		// list methods
		List<WebElement> elementss = driver.findElements(By.xpath("//*[@class='mat-option-text']"));

		System.out.println(elementss.size());

		Random rnd1 = new Random();

		int rndInt1 = rnd1.nextInt(elementss.size());

		System.out.println("Random Number : " + rndInt1);

		// select randomly any locality with any index wise
		elementss.get(rndInt1).click();
		
	
	}

	
	
	@Step("Select randomly any Area")
	public void validateselectinganyRandomArea() throws Exception {

		selectAreaXpath.click();

		// list methods
		List<WebElement> elements = driver.findElements(By.xpath("/html/body/div/div/div/div/mat-option"));

		System.out.println(elements.size());

		Random rnd = new Random();

		int rndInt = rnd.nextInt(elements.size());

		System.out.println("Random Number : " + rndInt);

		// select randomly any Route with any index wise
		elements.get(rndInt).click();

	}

	@Step("click on Search button")
	public void clickOnSearchButton() throws Exception {

		// check that search button is enabled then click it
		if (driver.findElement(By.xpath("/html/body/app-root/app-admin-layout/div"
				+ "/div/app-search-customer/div/div/div/div/form/div/button")).isEnabled()) {

			System.out.println(" Is display");

			// click on search button
			SearchButtonXpath.click();

		}

		else {

			System.out.println("Not display");

		}

	}

	@Step("click on list of customer")
	public void clickOnlistOfCustomerData() throws Exception {
		
		//Sorted data is display or not

		if(!driver.findElements(By.xpath("//td[@class='mat-cell cdk-column-phoneNo mat-column-phoneNo ng-star-inserted']")).isEmpty()) {
			  
				System.out.println("Popup is display");
					
				customerListXpath.click();
					
				} else {
					
					System.out.println("When popup is not display");
			
				}	
	
  	}	
	
	
	@Step("Click on one by one filed")
	
	public void clickonOneByOneFiled() throws Exception {
		

		// finding list of FAQ filed
		List<WebElement> elements = driver.findElements(
				By.xpath("//*[@class='mat-button-toggle-label-content']"));
		
		System.out.println("list count:" + elements.size());

		for (int i = 1; i < elements.size(); i++) {

			// click on horizontal text by one by one
			elements.get(i).click();
			
			Thread.sleep(4000);
			
			System.out.println("Index of i is :-" + Integer.toString(i));
			

			// one by one xpath
		
			String titlexpath="/html[1]/body[1]/app-root[1]/app-admin-layout[1]/div[1]/div[1]/app-customer-profile[1]/div[1]/div[1]/div[1]/div[1]/div[4]/mat-button-toggle-group[1]/mat-button-toggle[" + i + "]";
						
			WebElement nameText = driver.findElement(By.xpath(titlexpath));

			// printing title with description of FAQ 
			System.out.println("Field Name : " + nameText.getText().toString());	
		
		
	   } 
		   
		   
	   }
		
	
	@Step("Enter First Name amd Last Name")
	public void validatesEnterFirstandLastName(String name, String last) throws Exception {

			// creating Actions class for name
			Actions a = new Actions(driver);

			// click on first name filed
			FirstNameFiledXpath.click();

			// clear the text
			FirstNameFiledXpath.clear();

			Thread.sleep(1000);

			// click on first name filed
			FirstNameFiledXpath.click();
			
			// Enter a text
			a.sendKeys(name);

			a.perform();
			
			// creating Actions class for name
			Actions b = new Actions(driver);

			// click on last name filed
			LastNameFiledXpath.click();

			// clear the text
			LastNameFiledXpath.clear();

			Thread.sleep(1000);

			// click on first name filed
			LastNameFiledXpath.click();
			
			// Enter a text
			b.sendKeys(last);

			b.perform();
			
		}
	
	@Step("Enter positive wallet remark")
	public void validatesEnterPositiveWalletRemark(String Remark) throws Exception {

			// creating Actions class for name
			Actions a = new Actions(driver);

			// click on positive wallet remark filed
			postiveWalletRemarkFiledXpath.click();

			// clear the text
			postiveWalletRemarkFiledXpath.clear();

			Thread.sleep(1000);

			// click on positive wallet remark filed
			postiveWalletRemarkFiledXpath.click();
			
			// Enter a text
			a.sendKeys(Remark);

			a.perform();
			
	}
	
	@Step("Enter preference remark")
	public void validatesEnterPreferenceRemark(String Remark) throws Exception {

			// creating Actions class for name
			Actions a = new Actions(driver);

			// click on preference remark filed
			preferenceRemarkFiledXpath.click();

			// clear the text
			preferenceRemarkFiledXpath.clear();

			Thread.sleep(1000);

			// click on preference remark filed
			preferenceRemarkFiledXpath.click();
			
			// Enter a text
			a.sendKeys(Remark);

			a.perform();
			
	}
	
	@Step("Enter AddressLine1")
	public void validatesEnterAddressLine1(String address1) throws Exception {

			// creating Actions class for name
			Actions a = new Actions(driver);

			// click on Address line1 filed
			addressLine1FiledXpath.click();

			// clear the text
			addressLine1FiledXpath.clear();

			Thread.sleep(1000);

			// click on Address line1 filed
			addressLine1FiledXpath.click();
			
			// Enter a text
			a.sendKeys(address1);

			a.perform();
			
	}
	
	@Step("Enter AddressLine2")
	public void validatesEnterAddressLine2(String address2) throws Exception {

			// creating Actions class for name
			Actions a = new Actions(driver);

			// click on Address line1 filed
			addressLine2FiledXpath.click();

			// clear the text
			addressLine2FiledXpath.clear();

			Thread.sleep(1000);

			// click on Address line1 filed
			addressLine2FiledXpath.click();
			
			// Enter a text
			a.sendKeys(address2);

			a.perform();
			
	}
	
	
	@Step("Enter AddressLine3")
	public void validatesEnterAddressLine3(String address3) throws Exception {

			// creating Actions class for name
			Actions a = new Actions(driver);

			// click on Address line1 filed
			addressLine3FiledXpath.click();

			// clear the text
			addressLine3FiledXpath.clear();

			Thread.sleep(1000);

			// click on Address line1 filed
			addressLine3FiledXpath.click();
			
			// Enter a text
			a.sendKeys(address3);

			a.perform();
			
	}
	
	
	
	}
	

