package com.new_cms_admin.qa.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.new_cms_admin.qa.base.TestBase;
import io.qameta.allure.Step;

public class LoginPage extends TestBase {

	// Page Factory OR:

	@FindBy(xpath = "/html/body/app-root/app-login/div/div[1]/div[2]/div[1]/img")
	WebElement logoXpath;

	@FindBy(xpath = "//input[@placeholder='Enter username']")
	WebElement username_Xpath;

	@FindBy(xpath = "//input[@placeholder='Enter password']")
	WebElement password_Xpath;
	
	@FindBy(xpath="/html/body/app-root/app-login/div/div[1]/div[2]/div[2]/div/form/div[4]/button")
	WebElement loginButtonXpath;
	
	@FindBy(xpath="/html/body/app-root/app-admin-layout/div/div/app-dashboard/p")
	WebElement homePageTextXpath;
	
	

	// Initializing the page objects
	public LoginPage() {

		PageFactory.initElements(driver, this);
	}

	// Actions:

	@Step("Validate Login Page title")
	public String validateLoginPageTitle() throws Exception {

		return driver.getTitle();

	}
	
	@Step("Validate homepage Page text")
	public String validateHomePageText() throws Exception {

	   return homePageTextXpath.getText();

	}

	@Step("Verify Logo image")
	public boolean validateCDLogoImage() {

		return logoXpath.isDisplayed();

	}

	// Customer Details:

	@Step("Verify Login Credentials Details with Username and password: {0} ")
	public void loginCredentails(List<String> elements) throws Exception {

		// Array list for customer Details
		List<WebElement> xpaths = new ArrayList();

		// UserName section Xpath
		xpaths.add(username_Xpath);

		// Password section Xpath
		xpaths.add(password_Xpath);

		// creating Actions class
		Actions a = new Actions(driver);
		int i = 0;

		// creating for loop for Customer Details
		for (WebElement element : xpaths) {

			// clicking on element object
			element.click();

			// Get the element from the array list
			String eleString = elements.get(i);

			// Enter a text
			a.sendKeys(eleString);

			// click on enter keyword
			a.sendKeys(Keys.ENTER);

			a.perform();

			i += 1;

		}

	}
	
	@Step("click on Login button")
	public void clickOnLoginButton() throws Exception {

		loginButtonXpath.click();

	}
	
	@Step("Enter Username and password and login to the admin")
	public CustomerProfilePage login(String user, String pass) throws Exception {

		//Enter username
		username_Xpath.click();
		username_Xpath.sendKeys(user);
		
		//Enter password
		password_Xpath.click();
		password_Xpath.sendKeys(pass);
		
		//click on login button
		loginButtonXpath.click();
		return new CustomerProfilePage();
		

	}
	
	
	

}