package com.new_cms_admin.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.new_cms_admin.qa.base.TestBase;

import io.qameta.allure.Step;

public class SaleRechargeAnalysisPage extends TestBase {

	// Page Factory OR:

	@FindBy(xpath = "//p[normalize-space()='Analytics']")
	WebElement analysticsXpath;

	@FindBy(xpath = "//a[@href='/admin/analytics/saleRechargeAnalysis']")
	WebElement saleRechargeanalysticsXpath;

	@FindBy(xpath = "//input[@placeholder='Month']")
	WebElement MonthFiledXpath;

	@FindBy(xpath = "//input[@placeholder='Year']")
	WebElement YearFiledXpath;

	@FindBy(xpath = "//input[@placeholder='Select City']")
	WebElement SelectCityFiledXpath;

	@FindBy(xpath = "//input[@placeholder='Select franchise']")
	WebElement SelectFranchiseFiledXpath;

	@FindBy(xpath = "//input[@placeholder='Select locality']")
	WebElement SelectLocalityFiledXpath;

	@FindBy(xpath = "//input[@placeholder='Customer Id']")
	WebElement CustomerIdFiledXpath;

	@FindBy(xpath = "//input[@placeholder='Customer Name']")
	WebElement CustomerNameFiledXpath;

	@FindBy(xpath = "//input[@placeholder='Phone']")
	WebElement PhoneFiledXpath;

	@FindBy(xpath = "/html/body/app-root/app-admin-layout/div/div/app-sale-recharge-analysis/div/div/div/div/form/div/button[1]")
	WebElement getSaleRechargeButton;

	@FindBy(xpath = "//button[normalize-space()='Export']")
	WebElement exportButton;

	@FindBy(xpath = "//span[normalize-space()='July']")
	WebElement selectJulyXpath;

	@FindBy(xpath = "//span[normalize-space()='2021']")
	WebElement selectYear2021Xpath;

	@FindBy(xpath = "//span[normalize-space()='Gurugram']")
	WebElement SelectCityGurugram;

	@FindBy(xpath = "//span[normalize-space()='Gurgaon F and V']")
	WebElement selectFranchiseGFNVXpath;

	@FindBy(xpath = "//span[normalize-space()='Sector 48']")
	WebElement selectLocalitySectorXapth;
	
	@FindBy(xpath="//th[normalize-space()='Customer Id']")
	WebElement actualResultXpath;
	
	@FindBy(xpath="/html/body/div[2]/span[2]")
	WebElement actualResultForNotFoundXpath;

	

	// Initializing the page objects
	public SaleRechargeAnalysisPage() {

		
		
		PageFactory.initElements(driver, this);
	}
	

	// Actions:
	
	
	@Step(" verify Actual Result validation for list of data")
	public String ValidateActualResult() throws Exception {
		
		
		return actualResultXpath.getText();
		
	}
	
	@Step(" verify Actual Result validation for not found any list")
	public String ValidateActualResultforNotFoundData() throws Exception {
		
		
		return actualResultForNotFoundXpath.getText();
		
	}
	
	

	@Step("Click on Analysis and also click on sale recharge analysis link")
	public void clickonFirstAnalysisandClickOnGetSaleRechargLink() throws Exception {

		// click on analysis link
		analysticsXpath.click();

		// click on sale recharge analysis link
		saleRechargeanalysticsXpath.click();

	}

	@Step("click on get sale recharge button")
	public void clickOnGetSaleRechargeButton() throws Exception {

		// click on get sale recharge button
		getSaleRechargeButton.click();

	}

	@Step("click on export button")
	public void clickOnExportButton() throws Exception {

		// click on export button
		exportButton.click();

	}

	@Step("Enter a CustomerId")
	public void enterCustomerId(String custId) throws Exception {

		// creating Actions class
		Actions a = new Actions(driver);

		// click on customer id filed
		CustomerIdFiledXpath.click();

		// Enter a CustomerId
		a.sendKeys(custId);

		a.perform();

	}

	@Step("Enter a Customer Name")
	public void enterCustomerName(String custname) throws Exception {

		// creating Actions class
		Actions a = new Actions(driver);

		// click on customer name filed
		CustomerNameFiledXpath.click();

		// Enter a Customer name
		a.sendKeys(custname);

		a.perform();

	}

	@Step("Enter a Customer phone no")
	public void enterCustomerPhoneNumber(String phone) throws Exception {

		// creating Actions class
		Actions a = new Actions(driver);

		// click on customer phone filed
		PhoneFiledXpath.click();

		// Enter a Customer phone no.
		a.sendKeys(phone);

		a.perform();

	}

	@Step("Select any month from dropdown list")
	public void SelectDropdownListofMonth() throws Exception {

		// click on month filed
		MonthFiledXpath.click();

		// select july month
		selectJulyXpath.click();

	}

	@Step("Select any year from dropdown list")
	public void SelectDropdownListofYear() throws Exception {

		// click on year filed
		YearFiledXpath.click();

		// select 2021 year
		selectYear2021Xpath.click();

	}

	@Step("Select any city from dropdown list")
	public void SelectDropdownListofCity() throws Exception {

		// click on city filed
		SelectCityFiledXpath.click();

		// select city
		SelectCityGurugram.click();

	}

	@Step("Select any franchise from dropdown list")
	public void SelectDropdownListofFranchsie() throws Exception {

		// click on franchise filed
		SelectFranchiseFiledXpath.click();

		// select franchsie
		selectFranchiseGFNVXpath.click();

	}

	@Step("Select any  locality from dropdown list")
	public void SelectDropdownListofLocality() throws Exception {

		// click on locality filed
		SelectLocalityFiledXpath.click();

		// select locality
		selectLocalitySectorXapth.click();

	}

}
