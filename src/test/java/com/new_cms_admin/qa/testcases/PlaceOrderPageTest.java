package com.new_cms_admin.qa.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.new_cms_admin.qa.base.TestBase;
import com.new_cms_admin.qa.listeners.AllureReportListeners;
import com.new_cms_admin.qa.pages.CustomerProfilePage;
import com.new_cms_admin.qa.pages.LoginPage;
import com.new_cms_admin.qa.pages.PlaceOrderPage;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Link;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({ AllureReportListeners.class })
@Feature("Place Order Test")
@Epic("End To End Testing")
public class PlaceOrderPageTest extends TestBase {

	LoginPage loginPage;
	CustomerProfilePage customerProfilePage;
	PlaceOrderPage placeOrderPage;	

	


	public PlaceOrderPageTest() throws IOException {
		super();

	}

	@BeforeMethod(description = "Browser Start Up")
	public void setUp() throws Exception {

		initiallization();
		
		customerProfilePage =new CustomerProfilePage();

		loginPage = new LoginPage();
		
		placeOrderPage = new PlaceOrderPage();
		
		customerProfilePage=loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		

	}

	/*
	 * Customer place an order functionality
	 * 
	 * 
	 */

	@Test(priority = 0,description = "Verify that placing an order of milk product with qty=1 and also with Subscription type is Daily")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate placing an order of milk product with qty=1 and also with Subscription type is Daily")
	@Story("Story Name : To check placing an order of milk product with qty=1 and also with Subscription type is Daily")
	public void validatePlaceandOrderWithDaily() throws Exception {
	
		 Thread.sleep(5000);
		//click on search customer
		placeOrderPage.clickOnSearchCustomerSection();
		
		 Thread.sleep(8000);
		
		//enter phone no
		placeOrderPage.validatesSearchWithphoneNumber(prop.getProperty("ValidPhone"));
		
		Thread.sleep(5000);
		
		//click on search button
		placeOrderPage.clickOnSearchButton();
		
		Thread.sleep(2000);
		
		//click on list of data
		placeOrderPage.clickOnlistOfCustomerData();
		
		Thread.sleep(8000);
		
		//click on place order button
        placeOrderPage.clickonPlaceOrderButton();
        
        Thread.sleep(8000);
        
        //click on milk link
        placeOrderPage.SelectMilkProduct();
        
        Thread.sleep(3000);
        //enter a qty=1
        placeOrderPage.Enterqty("1");
        
        Thread.sleep(3000);
        
		//click on place order button
        placeOrderPage.ClickOnPlaceOrderButton();
        
        Thread.sleep(3000);
        //click on place order button in popup screen
        placeOrderPage.ClickOnPopUpPlaceOrderButton();
        
        Thread.sleep(5000);
        
		String expectedResult = placeOrderPage.validationmessageofSuccessfullyOrderPlace();
		String ActualResult= "Order has been successfully placed.";
		Assert.assertEquals(ActualResult, expectedResult,"It is getting an error message");

	}


	@AfterMethod(description = "Browser Tear Down")
	public void End() {
		
		driver.quit();
		driver=null;

	}

	
}

