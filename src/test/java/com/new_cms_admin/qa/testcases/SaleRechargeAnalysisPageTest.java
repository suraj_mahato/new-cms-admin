package com.new_cms_admin.qa.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.new_cms_admin.qa.base.TestBase;
import com.new_cms_admin.qa.listeners.AllureReportListeners;
import com.new_cms_admin.qa.pages.CustomerProfilePage;
import com.new_cms_admin.qa.pages.LoginPage;
import com.new_cms_admin.qa.pages.SaleRechargeAnalysisPage;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Link;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners({ AllureReportListeners.class })
@Feature("Sale Recharge Analysis Test")
@Epic("End To End Testing")
public class SaleRechargeAnalysisPageTest extends TestBase {

	LoginPage loginPage;
	SaleRechargeAnalysisPage saleRechargeAnalysisPage;
	CustomerProfilePage customerProfilePage;
	


	public SaleRechargeAnalysisPageTest() throws IOException {
		super();

	}

	@BeforeMethod(description = "Browser Start Up")
	public void setUp() throws Exception {

		initiallization();
		
		saleRechargeAnalysisPage =new SaleRechargeAnalysisPage();
		
		customerProfilePage =new CustomerProfilePage();

		loginPage = new LoginPage();
		
		customerProfilePage=loginPage.login(prop.getProperty("username"), prop.getProperty("password"));


		
		
	}

	/*
	 * Customer sale recharge analysis functionality
	 * 
	 * 
	 */
	
	
	@Test(enabled=false,priority = 0,description = "Verify that click on get sale recharge button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate that click on get sale recharge button")
	@Story("Story Name : To check click on get sale recharge button")
	public void validateListOfData() throws Exception {
		
		
		saleRechargeAnalysisPage.clickonFirstAnalysisandClickOnGetSaleRechargLink();
		
		Thread.sleep(5000);
		//click on get sale recharge button
		saleRechargeAnalysisPage.clickOnGetSaleRechargeButton();

		Thread.sleep(6000);

		String ActualResult = saleRechargeAnalysisPage.ValidateActualResult();
		Assert.assertEquals(ActualResult, "Customer Id", "Customer Id attribute is not display");
		
	}


	@Test(enabled=false,priority = 1,description = "Verify that Select a particular Month dropdown list and click on get sale recharge button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate that select a particular Month dropdown list and click on get sale recharge button")
	@Story("Story Name : To check select a particular Month dropdown list and click on get sale recharge button")
	public void validateSelectMonthDropdownListPage() throws Exception {
		
		
		saleRechargeAnalysisPage.clickonFirstAnalysisandClickOnGetSaleRechargLink();
		Thread.sleep(3000);
		
		//select dropdown list of month
		saleRechargeAnalysisPage.SelectDropdownListofMonth();
		
		Thread.sleep(2000);
		//click on get sale recharge button
		saleRechargeAnalysisPage.clickOnGetSaleRechargeButton();

		Thread.sleep(6000);
		
		String ActualResult = saleRechargeAnalysisPage.ValidateActualResult();
		Assert.assertEquals(ActualResult, "Customer Id", "Customer Id attribute is not display");

	}

	
	@Test(enabled=false,priority = 2,description = "Verify that Select a particular Year dropdown list and click on get sale recharge button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate that select a particular Year dropdown list and click on get sale recharge button")
	@Story("Story Name : To check select a particular Year dropdown list and click on get sale recharge button")
	public void validateSelectYearDropdownListPage() throws Exception {
		
		
		saleRechargeAnalysisPage.clickonFirstAnalysisandClickOnGetSaleRechargLink();
		Thread.sleep(3000);
		
		//select a particular year from drodown list
		saleRechargeAnalysisPage.SelectDropdownListofYear();
		
		Thread.sleep(2000);
		//click on get sale recharge button
		saleRechargeAnalysisPage.clickOnGetSaleRechargeButton();

		Thread.sleep(6000);
		
		String ActualResult = saleRechargeAnalysisPage.ValidateActualResult();
		Assert.assertEquals(ActualResult, "Customer Id", "Customer Id attribute is not display");

	}
	

	@Test(enabled=false,priority = 3,description = "Verify that Select a particular city from dropdown list and click on get sale recharge button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate that select a particular city from dropdown list and click on get sale recharge button")
	@Story("Story Name : To check select a particular city from dropdown list and click on get sale recharge button")
	public void validateSelectCityDropdownListPage() throws Exception {
		
		
		saleRechargeAnalysisPage.clickonFirstAnalysisandClickOnGetSaleRechargLink();
		Thread.sleep(3000);
		
		//select a particular City from drodown list
		saleRechargeAnalysisPage.SelectDropdownListofCity();
		
		Thread.sleep(2000);
		//click on get sale recharge button
		saleRechargeAnalysisPage.clickOnGetSaleRechargeButton();

		Thread.sleep(6000);
		
		String ActualResult = saleRechargeAnalysisPage.ValidateActualResult();
		Assert.assertEquals(ActualResult, "Customer Id", "Customer Id attribute is not display");

	}
	

	@Test(enabled=false,priority = 4,description = "Verify that Select a particular franchie from dropdown list and click on get sale recharge button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate that select a particular franchie from dropdown list and click on get sale recharge button")
	@Story("Story Name : To check select a particular franchie from dropdown list and click on get sale recharge button")
	public void validateSelectFranchiseDropdownListPage() throws Exception {
		
		
		saleRechargeAnalysisPage.clickonFirstAnalysisandClickOnGetSaleRechargLink();
		Thread.sleep(3000);
		
		//select a particular franchie from drodown list
		saleRechargeAnalysisPage.SelectDropdownListofFranchsie();
		
		Thread.sleep(2000);
		//click on get sale recharge button
		saleRechargeAnalysisPage.clickOnGetSaleRechargeButton();

		Thread.sleep(6000);
		
		String ActualResult = saleRechargeAnalysisPage.ValidateActualResult();
		Assert.assertEquals(ActualResult, "Customer Id", "Customer Id attribute is not display");

	}

	@Test(enabled=false,priority = 5,description = "Verify that Select a particular Locality from dropdown list and click on get sale recharge button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate that select a particular Locality from dropdown list and click on get sale recharge button")
	@Story("Story Name : To check select a particular Locality from dropdown list and click on get sale recharge button")
	public void validateSelectLocalityDropdownListPage() throws Exception {
		
		
		saleRechargeAnalysisPage.clickonFirstAnalysisandClickOnGetSaleRechargLink();
		Thread.sleep(3000);
		
		//select a particular Locality from drodown list
		saleRechargeAnalysisPage.SelectDropdownListofLocality();
		
		Thread.sleep(2000);
		//click on get sale recharge button
		saleRechargeAnalysisPage.clickOnGetSaleRechargeButton();

		Thread.sleep(6000);

		String ActualResult = saleRechargeAnalysisPage.ValidateActualResult();
		Assert.assertEquals(ActualResult, "Customer Id", "Customer Id attribute is not display");
		
	}
	
	@Test(priority = 6,description = "Verify that Enter a customerId and click on get sale recharge button")
	@Severity(SeverityLevel.NORMAL)
	@Link("https://admin.countrydelight.in/")
	@Description("Test Case Description: to validate that Enter a customerId and click on get sale recharge button")
	@Story("Story Name : To check Enter a customerId and click on get sale recharge button")
	public void validateEnterCustomerId() throws Exception {
		
		
		saleRechargeAnalysisPage.clickonFirstAnalysisandClickOnGetSaleRechargLink();
		Thread.sleep(10000);
		
		//Enter a customer Id
		saleRechargeAnalysisPage.enterCustomerId(prop.getProperty("CustomerId"));
		
		Thread.sleep(2000);
		//click on get sale recharge button
		saleRechargeAnalysisPage.clickOnGetSaleRechargeButton();

		Thread.sleep(5000);
		
	//	String ActualResults=saleRechargeAnalysisPage.ValidateActualResultforNotFoundData();
	
		String ActualResult = saleRechargeAnalysisPage.ValidateActualResult();
		
		
		String ExpectedResult="Customer Id";
				
	//	String ActualResults="Something went wrong! Please try again.";
		String ActualResults=saleRechargeAnalysisPage.ValidateActualResultforNotFoundData();
		
		System.out.println("Print: "+ActualResults);

	
		if (ActualResult.equalsIgnoreCase(ExpectedResult)) {

			System.out.println(" customer id text");
			Assert.assertEquals(ActualResult, "Customer Id");

		}
		
		else if(ActualResults.equalsIgnoreCase("No Sale Recharge Analysis found for selected criteria.")) {
			
			System.out.println(" it is getting an error");
			Assert.assertTrue(true,"Other data message" );
			
		}else {
			
			System.out.println("No Sale Recharge Analysis found for selected criteria.");
			Assert.assertTrue(false, "Something went wrong! Please try again.");
			
			
			
		}

	  

	}


	@AfterMethod(description = "Browser Tear Down")
	public void End() {
		
		driver.quit();
		driver=null;

	}

	
}
